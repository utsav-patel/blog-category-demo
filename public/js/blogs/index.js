$(document).ready(function() {
    var date = new Date();

    $('.date-datepicker').datepicker({
        todayBtn: 'linked',
        format: 'yyyy-mm-dd',
        autoclose: true,
    });

    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

    function fetch_data(from_date = '', to_date = '')
    {
        $.ajax({
            url:"/filter-daterange",
            method:"POST",
            data:{from_date:from_date, to_date:to_date, _token:token},
            dataType:"json",
            success:function(response)
            {
                if (!response.data.length) {
                    $("tr.blog-wrap").remove();
                    $('.header-tr').next('tr').html('<td colspan="5" class="text-center">No Blog Data Found.</td>');
                    $('.header-tr').closest('tr').after('<tr class="blog-wrap"><td colspan="5" class="text-center">No Blog Data Found.</td></tr>');
                } else {
                    $("tr.blog-wrap").remove();
                    var output = '';
                    for(var count = 0; count < response.data.length; count++) {
                        output += '<tr class="blog-wrap">';
                        output += '<td>' + (response.data[count].title ? response.data[count].title : '-') + '</td>';
                        output += '<td>' + (response.data[count].author ? response.data[count].author : '-') + '</td>';
                        output += '<td>' + (response.data[count].meta_title ? response.data[count].meta_title : '-') + '</td>';
                        output += '<td>' + (response.data[count].description ? response.data[count].description : '-') + '</td>';
                        output += '<td>' + (response.data[count].created_at ? response.data[count].created_at : '-') + '</td>';
                        output += '</tr>';
                    }
                    $('.header-tr').closest('tr').after(output);
                }
            }
        })
    }

    $('#filter').click(function() {
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(!from_date || !to_date) {
            alert('Please select both from date and to date.');
        } else if (from_date > to_date) {
            alert('Please select valid from date and to date.');
        } else {
            fetch_data(from_date, to_date);
        }
    });

    $('#refresh').click(function() {
        $('#from_date').val('');
        $('#to_date').val('');
        fetch_data();
    });
});