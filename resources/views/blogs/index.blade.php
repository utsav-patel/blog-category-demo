<title>Blogs</title>

@extends('layouts.master')

@section('content')
    @if($message = Session::get('success'))
        <div class="alert alert-success">
            {{ $message }}
        </div>
    @endif

    <div class="container mt-5">
        <h1 class="text-secondary mt-3 mb-4 text-center"><b>Blogs</b></h1>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col col-md-2"><b>Blogs Data</b></div>
                 <div class="col-md-6">
                    <div class="input-group input-daterange">
                        <input type="text" name="from_date" placeholder="From Date" id="from_date" readonly class="form-control date-datepicker" />&nbsp;&nbsp;
                        <input type="text"  name="to_date" placeholder="To Date" id="to_date" readonly class="form-control date-datepicker" />
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="button" name="filter" id="filter" class="btn btn-info btn-sm">Filter</button>
                    <button type="button" name="refresh" id="refresh" class="btn btn-warning btn-sm">Reset</button>
                </div>
                <div class="col col-md-2">
                    <a href="{{ route('blogs.create') }}" class="btn btn-success btn-sm float-end">New Blog</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr class="header-tr">
                    <th>Title</th>
                    <th>Author</th>
                    <th>Meta title</th>
                    <th>Description</th>
                    <th>Created At</th>
                </tr>
                @if(count($blogs))
                    @foreach($blogs as $blog)
                        <tr class="blog-wrap">
                            <td>{{ $blog->title ?? '-' }}</td>
                            <td>{{ $blog->author ?? '-' }}</td>
                            <td>{{ $blog->meta_title ?? '-' }}</td>
                            <td>{{ $blog->description ?? '-' }}</td>
                            <td>{{ $blog->created_at ?? '-' }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" class="text-center">No Blog Data Found.</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <script type="text/javascript" src="js/blogs/index.js"></script>
@endsection
