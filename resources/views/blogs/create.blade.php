<title>Add Blog</title>

@extends('layouts.master')
@section('content')
	@if($errors->any())
		<div class="alert alert-danger">
			<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
			</ul>
		</div>
	@endif

	<div class="container mt-5">
	    <h1 class="text-secondary mt-3 mb-4 text-center"><b>Add Blog</b></h1>
	</div>

	<div class="card">
		<div class="card-header">Add Blog</div>
		<div class="card-body">
			<form method="post" action="{{ route('blogs.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="row mb-3">
					<label class="col-sm-2 col-label-form">Title*</label>
					<div class="col-sm-10">
						<input type="text" name="title" id="title" class="form-control" />
					</div>
				</div>
				<div class="row mb-3">
					<label class="col-sm-2 col-label-form">Author*</label>
					<div class="col-sm-10">
						<input type="text" name="author" id="author" class="form-control" />
					</div>
				</div>
				<div class="row mb-3">
					<label class="col-sm-2 col-label-form">Meta title</label>
					<div class="col-sm-10">
						<input type="text" name="meta_title" id="meta_title" class="form-control" />
					</div>
				</div>
				<div class="row mb-3">
					<label class="col-sm-2 col-label-form">Description</label>
					<div class="col-sm-10">
						<textarea name="description" id="description" class="form-control"></textarea>
					</div>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-success">Add Blog</button>
				</div>
			</form>
		</div>
	</div>
@endsection('content')