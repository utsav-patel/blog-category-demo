<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Computer hardware',
            'blog_id' => 1
        ]);

        DB::table('categories')->insert([
            'name' => 'Electronics',
            'blog_id' => 1
        ]);

        DB::table('categories')->insert([
            'name' => 'Home daily',
            'blog_id' => 2
        ]);
    }
}
